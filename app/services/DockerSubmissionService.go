package services

import (
	"archive/tar"
	"bitbucket.org/YaroslavDev/online-judge-go/app/models"
	"bytes"
	"fmt"
	"github.com/fsouza/go-dockerclient"
	"github.com/op/go-logging"
)

type DockerSubmissionService struct {
	Client DockerClient
}

var log = logging.MustGetLogger("online-judge")

func (service DockerSubmissionService) ExecuteSubmission(submission models.Submission) (*models.ExecutionResult, error) {
	createOpts, hostConfig, filename := createContainerOptions(submission)
	container, err := service.client().CreateContainer(createOpts)
	if err != nil {
		return nil, err
	}

	// Upload tar archive with user submitted code to container
	err = service.uploadCodeToContainer(filename, submission.SourceCode, container.ID)
	if err != nil {
		return nil, err
	}

	err = service.client().StartContainer(container.ID, &hostConfig)
	if err != nil {
		return nil, err
	}

	status, err := service.client().WaitContainer(container.ID)
	if err != nil {
		return nil, err
	}

	stdout, stderr, err := service.getLogs(container.ID)
	if err != nil {
		return nil, err
	}

	log.Info("Executing submission: %s", submission)
	execResult := models.ExecutionResult{Status: status, Stdout: stdout, Stderr: stderr}
	log.Info("Execution results: \n")
	log.Info("Stdout: \n")
	log.Info(execResult.Stdout)
	log.Info("Stderr: \n")
	log.Info(execResult.Stderr)

	return &execResult, nil
}

func createContainerOptions(submission models.Submission) (docker.CreateContainerOptions, docker.HostConfig, string) {
	hostConfig := docker.HostConfig{PublishAllPorts: true}
	var bashCommand, image, filenameWithExtension string
	var cmd []string
	switch submission.Language {
	case "java":
		image = "java"
		filenameWithExtension = fmt.Sprintf("%s.java", submission.Filename)
		bashCommand = fmt.Sprintf("javac %s; java %s", filenameWithExtension, submission.Filename)
		cmd = []string{"bash", "-c", bashCommand}
	}
	createOpts := docker.CreateContainerOptions{
		Config: &docker.Config{
			Image: image,
			Cmd:   cmd,
		},
		HostConfig: &hostConfig,
	}
	log.Info("Container: %s", createOpts.Config.Image)
	log.Info("Command: %s", createOpts.Config.Cmd)
	return createOpts, hostConfig, filenameWithExtension
}

func (service DockerSubmissionService) uploadCodeToContainer(filename, sourceCode, containerID string) error {
	// Create a buffer to write our archive to.
	buf := new(bytes.Buffer)
	// Create a new tar archive.
	tw := tar.NewWriter(buf)
	hdr := &tar.Header{
		Name: filename,
		Mode: 0600,
		Size: int64(len(sourceCode)),
	}
	err := tw.WriteHeader(hdr)
	if err != nil {
		return err
	}
	_, err = tw.Write([]byte(sourceCode))
	if err != nil {
		return err
	}
	reader := bytes.NewReader(buf.Bytes())
	uploadOpts := docker.UploadToContainerOptions{
		InputStream: reader,
		Path:        "/",
	}
	err = service.client().UploadToContainer(containerID, uploadOpts)
	return err
}

func (service DockerSubmissionService) getLogs(containerID string) (string, string, error) {
	var stdout, stderr bytes.Buffer
	logsOpts := docker.LogsOptions{
		Container:    containerID,
		OutputStream: &stdout,
		ErrorStream:  &stderr,
		Stdout:       true,
		Stderr:       true,
	}
	err := service.client().Logs(logsOpts)
	if err != nil {
		return "", "", err
	}
	return stdout.String(), stderr.String(), nil
}

func (service DockerSubmissionService) client() DockerClient {
	if service.Client == nil {
		client, err := docker.NewClientFromEnv()
		panicIfError(err)
		service.Client = client
	}
	return service.Client
}

func panicIfError(err error) {
	if err != nil {
		panic(err)
	}
}
