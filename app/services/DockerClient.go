package services

import "github.com/fsouza/go-dockerclient"

type DockerClient interface {
	CreateContainer(docker.CreateContainerOptions) (*docker.Container, error)
	UploadToContainer(string, docker.UploadToContainerOptions) error
	StartContainer(string, *docker.HostConfig) error
	WaitContainer(string) (int, error)
	Logs(docker.LogsOptions) error
	KillContainer(docker.KillContainerOptions) error
}
