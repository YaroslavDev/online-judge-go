package services

import (
	"bytes"
	"encoding/json"
	"github.com/revel/revel"
)

type Utils struct{}

func (u Utils) UnmarshalRequestBody(request *revel.Request, obj interface{}) error {
	buffer := new(bytes.Buffer)
	buffer.ReadFrom(request.Body)
	return json.Unmarshal(buffer.Bytes(), obj)
}
