package services

import "bitbucket.org/YaroslavDev/online-judge-go/app/models"

type SubmissionService interface {
	ExecuteSubmission(submission models.Submission) (*models.ExecutionResult, error)
}
