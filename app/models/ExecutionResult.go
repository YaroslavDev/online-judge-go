package models

import (
	"encoding/json"
)

type ExecutionResult struct {
	Status int
	Stdout string
	Stderr string
}

func (j ExecutionResult) String() string {
	js, _ := json.Marshal(j)
	return string(js)
}
