package models

import "encoding/json"

type Submission struct {
	Language   string
	Filename   string
	SourceCode string
}

func (s Submission) String() string {
	js, _ := json.Marshal(s)
	return string(js)
}
