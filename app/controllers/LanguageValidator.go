package controllers

type LanguageValidator struct {}

func (sv LanguageValidator) IsSatisfied(obj interface{}) bool {
	switch obj {
	case "java":
		return true
	default:
		return false
	}
}

func (sv LanguageValidator) DefaultMessage() string {
	return "Supported languages: java"
}