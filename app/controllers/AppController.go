package controllers

import (
	"bitbucket.org/YaroslavDev/online-judge-go/app/models"
	"bitbucket.org/YaroslavDev/online-judge-go/app/services"
	"github.com/op/go-logging"
	"github.com/revel/revel"
	"net/http"
)

type AppController struct {
	*revel.Controller
	services.Utils
	SubmissionService services.SubmissionService
}

var log = logging.MustGetLogger("online-judge")

func (c *AppController) Index() revel.Result {
	return c.Render()
}

func (app *AppController) PostSubmission() revel.Result {
	var submission models.Submission
	app.UnmarshalRequestBody(app.Request, &submission)
	app.Validation.Required(submission.Language)
	app.Validation.Required(submission.Filename)
	app.Validation.Required(submission.SourceCode)
	app.Validation.Check(submission.Language, &LanguageValidator{})
	if app.Validation.HasErrors() {
		log.Error("Validation failed!")
		log.Error("%s", app.Validation.Errors)
		app.Response.WriteHeader(http.StatusBadRequest, "application/json")
		return app.RenderJson(app.Validation.Errors)
	}
	log.Info("Received submission: %s", submission)
	execResult, err := app.submissionService().ExecuteSubmission(submission)
	if err != nil {
		log.Error("Error: %s", err)
		app.Response.WriteHeader(http.StatusInternalServerError, "application/json")
		return app.RenderJson("Could not process your request!")
	} else {
		return app.RenderJson(execResult)
	}
}

func (app *AppController) submissionService() services.SubmissionService {
	if app.SubmissionService == nil {
		app.SubmissionService = &services.DockerSubmissionService{}
	}
	return app.SubmissionService
}
