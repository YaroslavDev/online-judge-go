package tests

import "github.com/revel/revel/testing"

type AppControllerIT struct {
	testing.TestSuite
}

func (t *AppControllerIT) Before() {
	println("Set up")
}

func (t *AppControllerIT) TestThatIndexPageWorks() {
	t.Get("/")
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
}

func (t *AppControllerIT) After() {
	println("Tear down")
}
