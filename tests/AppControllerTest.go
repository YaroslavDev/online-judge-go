package tests

import (
	"bitbucket.org/YaroslavDev/online-judge-go/app/controllers"
	"bitbucket.org/YaroslavDev/online-judge-go/app/models"
	"bytes"
	"encoding/json"
	"github.com/revel/revel"
	"github.com/revel/revel/testing"
	"io/ioutil"
	"net/http"
)

type AppControllerTest struct {
	testing.TestSuite
}

func (t *AppControllerTest) TestPostSubmissionPositive() {
	submission := models.Submission{Language: "java", SourceCode: "public class HelloWorld { public static void main(String[] args) { System.out.println(\"Hello, Radnitz!\"); System.exit(0);  } }"}
	body, _ := json.Marshal(submission)
	request := &revel.Request{
		ContentType: "application/json",
		Request: &http.Request{
			Body: ioutil.NopCloser(bytes.NewReader(body)),
		},
	}
	responseBuffer := &bytes.Buffer{}
	responseWriter := ResponseWriterMock{Headers: http.Header{}, Buffer: responseBuffer}
	response := &revel.Response{Status: 0, Out: responseWriter}
	appController := controllers.AppController{
		SubmissionService: SubmissionServiceMock{Test: t},
		Controller: &revel.Controller{
			Request:    request,
			Response:   response,
			Validation: &revel.Validation{},
		},
	}

	result := appController.PostSubmission()

	jsonResult, ok := result.(revel.RenderJsonResult)
	t.Assert(ok)
	jsonResult.Apply(request, response)
	responseBody := responseBuffer.String()
	var execResult models.ExecutionResult
	json.Unmarshal([]byte(responseBody), &execResult)
	t.AssertEqual(0, execResult.Status)
	t.AssertEqual("Hello, Radnitz!", execResult.Stdout)
	t.AssertEqual("", execResult.Stderr)
}

type SubmissionServiceMock struct {
	Test *AppControllerTest
}

func (service SubmissionServiceMock) ExecuteSubmission(submission models.Submission) (*models.ExecutionResult, error) {
	return &models.ExecutionResult{Status: 0, Stdout: "Hello, Radnitz!"}, nil
}

type ResponseWriterMock struct {
	Headers http.Header
	Buffer  *bytes.Buffer
}

func (r ResponseWriterMock) Header() http.Header {
	return r.Headers
}

func (r ResponseWriterMock) Write(in []byte) (int, error) {
	r.Buffer.Write(in)
	return len(in), nil
}

func (r ResponseWriterMock) WriteHeader(int) {}
