package tests

import (
	"bitbucket.org/YaroslavDev/online-judge-go/app/models"
	"bitbucket.org/YaroslavDev/online-judge-go/app/services"
	"github.com/fsouza/go-dockerclient"
	"github.com/revel/revel/testing"
)

type SubmissionServiceTest struct {
	testing.TestSuite
}

func (t *SubmissionServiceTest) TestExecuteSubmission() {
	submissionService := services.DockerSubmissionService{Client: &DockerClientMock{Test: t, ContainerId: "123"}}
	submission := models.Submission{Language: "java", SourceCode: ""}

	execResult, err := submissionService.ExecuteSubmission(submission)

	t.AssertEqual(err, nil)
	t.AssertEqual(execResult.Status, 0)
	t.AssertEqual(execResult.Stdout, "")
	t.AssertEqual(execResult.Stderr, "")
}

type DockerClientMock struct {
	Test        *SubmissionServiceTest
	ContainerId string
}

func (client DockerClientMock) CreateContainer(docker.CreateContainerOptions) (*docker.Container, error) {
	container := &docker.Container{ID: client.ContainerId}
	return container, nil
}

func (client DockerClientMock) UploadToContainer(id string, opts docker.UploadToContainerOptions) error {
	client.Test.AssertEqual(id, client.ContainerId)
	return nil
}

func (client DockerClientMock) StartContainer(id string, hostConfig *docker.HostConfig) error {
	client.Test.AssertEqual(id, client.ContainerId)
	return nil
}

func (client DockerClientMock) WaitContainer(id string) (int, error) {
	client.Test.AssertEqual(id, client.ContainerId)
	return 0, nil
}

func (client DockerClientMock) Logs(opts docker.LogsOptions) error {
	client.Test.AssertEqual(opts.Container, client.ContainerId)
	return nil
}

func (client DockerClientMock) KillContainer(opts docker.KillContainerOptions) error {
	client.Test.AssertEqual(opts.ID, client.ContainerId)
	return nil
}
